
-- SUMMARY --

Display form ids and associated file or path references.

* Does not display concatenated or embedded variable form ids.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

None.


-- USAGE --

* Visit admin/config/development/form-ids


-- TROUBLESHOOTING --

* In order to avoid exhausting the memory, you may need to increase the value
  of php.ini's memory_limit variable. Visit admin/reports/status/php to determine
  the current memory_limit value as well as the location of the loaded configuration.
